package com.example.football;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Integer countFirst = 0;
    private Integer countSecond = 0;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView counterView = (TextView) findViewById(R.id.txt_counters);
        if (savedInstanceState != null) {
            countFirst = savedInstanceState.getInt("countFirst");
            countSecond = savedInstanceState.getInt("countSecond");
            counterView.setText(countFirst + ":" + countSecond);
        } else {
            counterView.setText(countFirst + ":" + countSecond);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("countFirst", countFirst);
        outState.putInt("countSecond", countSecond);
    }

    @SuppressLint("SetTextI18n")
    public void onClickFirstFlag(View view) {
        TextView counterView = (TextView) findViewById(R.id.txt_counters);
        countFirst++;
        counterView.setText(countFirst + ":" + countSecond);
    }

    @SuppressLint("SetTextI18n")
    public void onClickSecondFlag(View view) {
        TextView counterView = (TextView) findViewById(R.id.txt_counters);
        countSecond++;
        counterView.setText(countFirst + ":" + countSecond);
    }

    public void onClickReset(View view) {
        TextView counterView = (TextView) findViewById(R.id.txt_counters);
        countFirst = 0;
        countSecond = 0;
        counterView.setText("0:0");
    }
}